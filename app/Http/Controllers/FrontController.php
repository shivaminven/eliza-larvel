<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
    	$data['bodyclass']="Login";
    	return view('frontend.login',$data);
    }
}

