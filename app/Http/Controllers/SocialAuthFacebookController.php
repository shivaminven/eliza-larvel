<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Socialite;

class SocialAuthFacebookController extends Controller
{
    //
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback($provider,Request $request)
    {
        try{
            $user = Socialite::driver($provider)->user();
            $name = $user->name;
            $email = $user->email;
            $appid = $user->id;
            $oauth_token=$user->token;
            return redirect('/welcome');
        } catch (Exception $e) {
            return redirect('auth/linkedin');
        }
    }
   

    
    
}
