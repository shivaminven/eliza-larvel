
(function ($) {
    "use strict";
    var dt = new Date();
dt.setFullYear(new Date().getFullYear()-18);
    $( ".datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        viewMode: "years", 
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        autoclose: true,
        endDate : dt,
        format: 'dd-M-yyyy',
    });

    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })

    /*=====================================================*/
    $('#codeinput').on('change',function(){
        checkInviteCode($(this).val());
        $('#codeinput').addClass('has-val');
    });
    $('#codeinput').on('keyup',function(){
        if($(this).val().length>8){
            checkInviteCode($(this).val());  
            $('#codeinput').addClass('has-val');
        }
        
    });
    // $(document).ready(function(){
    //     
    // })
    function checkInviteCode(value,code){
        $('#displsts').hide();
        $('#displsts').removeAttr('style');
        $.ajax({
            type: "POST",
            url: "/new_modules/harmohannew/checkCode",
            data:  {"_token": $('#csrftoken').val(),'code': value},
            success: function(result){
                var obj = JSON.parse(result);
                if(obj.valid_code==1 && obj.use_for_sign_up==1){
                    $('#displsts').html('Code Valid');
                    $('#displsts').show();
                    $('#displsts').attr('style',"display:block !important;");
                    $('#displsts').removeAttr('class');
                    $('#displsts').addClass('pop-success');
                    $('#codecheck i').removeAttr('class');
                    $('#codecheck i').removeAttr('class');
                    $('#codecheck i').attr('class','fa fa-check');
                    $('#sendcodebtns').removeAttr('disabled');


                }
                else{
                    var a=obj.no_use_reason; 
                    if(a===""){
                        a="Invalid Code";
                    }
                    $('#displsts').html(a);
                    $('#displsts').attr('style',"display:block !important;");
                    $('#displsts').removeAttr('class');
                    $('#displsts').addClass('pop-warning');
                    $('#codecheck i').removeAttr('class');
                    $('#codecheck i').attr('class','fa fa-times');
                    $('#sendcodebtns').attr('disabled','disabled');
                }
            }
        })
    }
    
    $('#emailcheck').on('keyup',function(){
        var email=$(this).val();
        if(IsEmail(email)==true){
            $.ajax({
                type: "POST",
                url: "/new_modules/harmohannew/checkExistEmail/"+ email,
                data:  {"_token": $('#csrftoken').val()},
                success: function(result){
                    var obj = JSON.parse(result);
                    if(obj.use_for_sign_up==1){
                        $('#displests').html('Email Available');
                        $('#displests').show();
                        $('#displests').attr('style',"display:block !important;");
                        $('#displests').removeAttr('class');
                        $('#displests').addClass('pop-success');
                        $('#codecheck i').removeAttr('class');
                        $('#codecheck i').attr('class','fa fa-check');
                        $('#pe_val').val('1');
                        $('#sendcodebtns').removeAttr('disabled');

                    }
                    else{
                        $('#displests').html('Email Exist');
                        $('#displests').show();
                        $('#displests').attr('style',"display:block !important;");
                        $('#displests').removeAttr('class');
                        $('#displests').addClass('pop-warning');
                        $('#codecheck i').removeAttr('class');
                        $('#codecheck i').attr('class','fa fa-times');
                        $('#pe_val').val('0');
                        $('#sendcodebtns').attr('disabled',"disabled");
                    }
                }
            })
        }else{
            $('#displests').html('Invaild Email');
            $('#displests').show();
            $('#displests').attr('style',"display:block !important;");
            $('#displests').removeAttr('class');
            $('#displests').addClass('pop-warning');
            $('#codecheck i').removeAttr('class');
            $('#codecheck i').attr('class','fa fa-times');
            $('#sendcodebtns').attr('disabled','disabled');
            $('#pe_val').val('0');
        }
    });

    $('#joinfullname').on('change',function(){
        var fullname=$(this).val();
        validatenamevalue(fullname);
        
    })
    $('#joinfullname').on('keyup',function(){
        var fullname=$(this).val().replace(/ /g,"-");
        $('#usercheck').val(fullname);
        checkUserName(fullname);
    })

    $('#usercheck').on('keyup',function(){
        var username=$(this).val().replace(/ /g,"-");
        $(this).val(username);
        checkUserName(username);
    });

    $('#codeemailcheck').on('keyup',function(){
        var email=$(this).val();
        if(IsEmail(email)==true){
            $.ajax({
                type: "POST",
                url: "/new_modules/harmohannew/checkExistEmail/"+ email,
                data:  {"_token": $('#csrftoken').val()},
                success: function(result){
                    var obj = JSON.parse(result);
                    if(obj.use_for_sign_up==0){
                        $('#displests').html('Email Available');
                        $('#displests').show();
                        $('#displests').attr('style',"display:block !important;");
                        $('#displests').removeAttr('class');
                        $('#displests').addClass('pop-success');
                        $('#codecheck i').removeAttr('class');
                        $('#codecheck i').attr('class','fa fa-check');
                        $('#sendcodebtns').removeAttr('disabled');

                    }
                    else{
                        $('#displests').html('Email Exist');
                        $('#displests').show();
                        $('#displests').attr('style',"display:block !important;");
                        $('#displests').removeAttr('class');
                        $('#displests').addClass('pop-warning');
                        $('#codecheck i').removeAttr('class');
                        $('#codecheck i').attr('class','fa fa-times');
                        $('#sendcodebtns').attr('disabled','disabled');
                    }
                }
            })
        }else{
            $('#displests').html('Invaild Email');
            $('#displests').show();
            $('#displests').attr('style',"display:block !important;");
            $('#displests').removeAttr('class');
            $('#displests').addClass('pop-warning');
            $('#codecheck i').removeAttr('class');
            $('#codecheck i').attr('class','fa fa-times');
            $('#sendcodebtns').attr('disabled','disabled');
        }
    });

    $('#passwordfld').on('keyup',function(){
        var pass = $(this).val();
        var strength = 1;
        var arr = [/.{5,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/];
        jQuery.map(arr, function(regexp) {
          if(pass.match(regexp))
             strength++;
        });
        $(".p_sm").removeClass('p-red p-yellow p-d-yellow p-green');
        $('#password-strength-text span').removeClass('p-red p-yellow p-d-yellow p-green');
        if(strength<=2){
            $("#password-strength-meter1").addClass("p-red");
            $('#ps_val').val('0');
            $('#password-strength-text span').addClass('p-red');
            $('#password-strength-text span').text('Very Weak');
            $('#password-strength-text').attr('style',"display:block !important;");
        }
        else if(strength==3){
            $("#password-strength-meter1").addClass("p-yellow");
            $("#password-strength-meter2").addClass("p-yellow");
            $('#ps_val').val('0');
            $('#password-strength-text span').addClass('p-yellow');
            $('#password-strength-text span').text('Weak');
            $('#password-strength-text').attr('style',"display:block !important;");
        }
        else if(strength==4){
            $("#password-strength-meter1").addClass("p-d-yellow");
            $("#password-strength-meter2").addClass("p-d-yellow");
            $("#password-strength-meter3").addClass("p-d-yellow");
            $('#ps_val').val('1');
            $('#password-strength-text span').addClass('p-d-yellow');
            $('#password-strength-text span').html('Medium');
            $('#password-strength-text').attr('style',"display:block !important;");
        }
        else if(strength==5){
            $("#password-strength-meter1").addClass("p-green");
            $("#password-strength-meter2").addClass("p-green");
            $("#password-strength-meter3").addClass("p-green");
            $("#password-strength-meter4").addClass("p-green");
            $('#ps_val').val('1');
            $('#password-strength-text span').addClass('p-green');
            $('#password-strength-text span').html('Strong');
            $('#password-strength-text').attr('style',"display:block !important;");
        }
        $('#password-strength-meter').attr('value',strength);
    });
    $('#ownPlaces').on('keyup',function(){
        $('#flgspn').html('<i class="fa fa-map" aria-hidden="true"></i>');
        
    });
    
  
    $('.fa-eye-slash').on('click',function(){
        $('#passwordfld').removeAttr('type');
        $('#passwordfld').attr('type','text');
        $('.fa-eye-slash').addClass('fa-eye');
        $('.fa-eye-slash').toggleClass('p_s');
        $('.fa-eye-slash').attr('onclick',"hidepassword()");
        $('.fa-eye-slash').toggleClass('fa-eye-slash');

    });

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });


    $('#jnfrm').on("submit",function() {
        //e.preventDefault();
        if($('#ps_val').val()==="1" && $('#pn_val').val()==="1" && $('#pe_val').val()==="1" && $('#pu_val').val()==="1" && $('#pl_val').val()==="1"){
            return true;
        }else{
            return false;
        }

    });

    

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            return false;
        }else{
            return true;
        }
    }

    function validatenamevalue(name){
        $('#displnsts').attr('style',"display:none !important;");
        $('#ncodecheck i').removeAttr('class');
        $('#ncodecheck i').attr('class','fa fa-user');
        if(name == '' || name == null) {
            $('#displnsts').html('Enter Full Name');
            $('#displnsts').show();
            $('#displnsts').attr('style',"display:block !important;");
            $('#displnsts').removeAttr('class');
            $('#displnsts').addClass('pop-success');
            $('#ncodecheck i').removeAttr('class');
            $('#pn_val').val('0');
            $('#ncodecheck i').attr('class','fa fa-times');
        } else if(name.indexOf(' ') == -1 ||(name.indexOf(' ') > 0 && name.indexOf(' ') == name.length-1)) {
            $('#displnsts').html('Enter Last Name');
            $('#displnsts').show();
            $('#displnsts').attr('style',"display:block !important;");
            $('#displnsts').removeAttr('class');
            $('#displnsts').addClass('pop-warning');
            $('#ncodecheck i').removeAttr('class');
            $('#ncodecheck i').attr('class','fa fa-times');
            $('#pn_val').val('0');
            $('#nsendcodebtns').attr('disabled','disabled');
        }else{
            $('#pn_val').val('1');
        }
    }

    function checkUserName(cname){
        $.ajax({
                type: "POST",
                url: "/new_modules/harmohannew/checkExistUsername/"+ cname,
                data:  {"_token": $('#csrftoken').val()},
                success: function(result){
                    var obj = JSON.parse(result);
                    if(obj.valid_username==1){
                        $('#displunsts').html('Username Available');
                        $('#displunsts').show();
                        $('#displunsts').attr('style',"display:block !important;");
                        $('#displunsts').removeAttr('class');
                        $('#displunsts').addClass('pop-success');
                        $('#usercod i').removeAttr('class');
                        $('#usercod i').attr('class','fa fa-check');
                        $('#pu_val').val('1');
                        $('#sendcodebtns').removeAttr('disabled');
                    }
                    else{
                        $('#displunsts').html('Username Not Available');
                        $('#displunsts').show();
                        $('#displunsts').attr('style',"display:block !important;");
                        $('#displunsts').removeAttr('class');
                        $('#displunsts').addClass('pop-warning');
                        $('#usercod i').removeAttr('class');
                        $('#usercod i').attr('class','fa fa-times');
                        $('#pu_val').val('0');
                        $('#sendcodebtns').attr('disabled','disabled');
                    }
                }
            })
    }

//     $('body').on('click', function (e) {
//     $('[data-toggle="popover"]').each(function () {
//         //the 'is' for buttons that trigger popups
//         //the 'has' for icons within a button that triggers a popup
//         if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
//             $(this).popover('hide');
//         }
//     });
// });


    $(document).ready(function(){
      $('[data-toggle="popover"]').popover();



        $('#categorycts').selectpicker('show');
        $.ajax({
            type: "GET",
            url: "/new_modules/harmohannew/getCountry",
            success: function(result){
                var obj = JSON.parse(result);
                var city=obj.city;
                var region=obj.region;
                var country=obj.country;
                var flag=obj.flag;
                var countrycode=obj.country_code;
                var region_code=obj.region_code;
                $('#ownPlaces').val(city+", "+country);
                $('#withinregions').val(city+", "+country);
                $('#countrycodeget').val(countrycode);
                $('.getlc').text(country);
                $('.getlcwith').text("Within  "+country);
                $('.getlcout').text("Outside of  "+country);
                $('#citygg').val(city);
                $('#stategg').val(region);
                $('#stateabrgg').val(region_code);
                $('#fulladdressgg').val(city+", "+region+", "+country);
                $('#ownPlaces').addClass('has-val');
                $('#flgspn').html("<img src='"+flag+"'/>");
                $('#gglatitude').val(obj.latitue);
                $('#gglongitude').val(obj.longitude);
                initMap(obj.latitue,obj.longitude);
            }
        })
       showScreen(1);
       getAllCategory();
       getAllCountry();
       // Initialize and add the map

       jobTimer();
    });

    $("#notifiactiondrops").scroll(function(e){
    	alert();
    });
    $(document).on("click", ".profsubfunctions", function(event){
        var arr = [];
        var lowfunid=$(this).attr('data-funcid');
        var lowfunname=$(this).attr('data-funcname');
        var mainCat=$(this).attr('data-maincatid');
        var mainCatname=$(this).attr('data-maincatname');
        var subcatid=$(this).attr('data-subcatid');
        arr.push({id:lowfunid,name:lowfunname,subcatid:subcatid});
        $(this).parent('li').toggleClass('pro-active');
        $(this).children('i').toggle();
        $(this).append()
        if ( $(this).parent('li').hasClass("pro-active") ){

            bindCarosal(arr,lowfunname,mainCat,mainCatname);
        }
        else{
            removeProfessionItem(lowfunid,mainCat);
        }
    });
    function bindCarosal(arr,lowfunname,maincatid,maincatname)
    {
        $('#categories-slider').removeAttr('style');
        $('#categories-slider').attr('style','display:block !important;');
        var ind=$('.item-categories').length;
        var l =$('#mainprof'+maincatid+" ul li").length;
        var totalel= l+1;
        if($('#mainprof'+maincatid).length==0){
            var catdivhtml='';
            //$('#categories-slider').append(catdivhtml).trigger('refresh.owl.carousel');
            $('#categories-slider').trigger('add.owl.carousel', ['<div class="item-categories" id="mainprof'+maincatid+'" dvar="'+ind+'"><div class="l-panel"><input type="hidden" name="professions['+ind+'][low_id]" value="'+maincatid+'"><input type="hidden" name="professions['+ind+'][category_id]" value="'+arr[0].subcatid+'"><h4>'+maincatname+'</h4><div class="c_holder"><ul><li id="proitm'+arr[0].id+'"><input type="hidden" name="professions['+ind+'][low_functions][][low_function_id]" value="'+arr[0].id+'"><div class="card c-pill"><span class="close-icon deletelistitem" data-effect="fadeOut" datav="'+arr[0].id+'" datama="'+maincatid+'"><i class="fa fa-times-circle"></i></span><span>'+lowfunname+'</span></div></li></ul></div><div class="i-foot"><div class="row"><div class="col-md-6 col-sm-6 col-xs-12 col-det"><span class="col-selected"></span></div><div class="col-md-6 col-sm-6 col-xs-12 col-act"><a class="btn btn-primary updateprof" datamainids="'+maincatid+'" datamainnm="'+maincatname+'">Update</a></div></div></div></div></div>']).trigger('refresh.owl.carousel');
        }
        else{
            var ins = $('#mainprof'+maincatid).attr('dvar');
            $('#mainprof'+maincatid+" ul").append('<li id="proitm'+arr[0].id+'"><input type="hidden" name="professions['+ins+'][low_functions][][low_function_id]" value="'+arr[0].id+'"><div class="card c-pill"><span class="close-icon deletelistitem" data-effect="fadeOut" datav="'+arr[0].id+'" datama="'+maincatid+'"><i class="fa fa-times-circle"></i></span><span>'+lowfunname+'</span></div></li>');
        }
        $('#mainprof'+maincatid+" .col-selected").html(totalel+' Selected');
    }
    
    function removeProfessionItem(listid,maincatid){
        $('#proitm'+listid).remove();
        var l =$('#mainprof'+maincatid+" ul li").length;
        $('#mainprof'+maincatid+" .col-selected").html(l+' Selected');
        if(l<1){
            var listItem = $('#mainprof'+maincatid).parent('.owl-item');
            var indexToRemove =$( ".owl-item" ).index( listItem );
            $("#categories-slider").trigger('remove.owl.carousel', [indexToRemove]).trigger('refresh.owl.carousel');
            var ln =$('.owl-item').length;
            if(ln<1){
                $('#categories-slider').removeAttr('style');
                $('#categories-slider').attr('style','display:none !important;');
            }
        }
    }

    $('#fullcarddisplay').on('click',function(){
        alert();
        $('#fullcard').removeAttr('style');
        $('#fullcard').attr('style','display:block');
        $('#cardswipe').removeAttr('style');
        $('#cardswipe').attr('style','display:none');
    })

    $('#checkbox-toggle7').on("click",function(){
        if($(this).prop("checked") == true){
            $('.nationalall').hide();
            $('#nmmini').val($("#lmmini").val());
            $('#nmpre').val($("#lmpre").val());
            $('#nymini').val($("#lymini").val());
            $('#nypre').val($("#lypre").val());
            $('#nhmini').val($("#lhmini").val());
            $('#nhpref').val($("#lhpre").val());
            if($("#checkbox-toggle6").prop("checked") == true){
                $('#checkbox-toggle8').attr("checked","checked");
                $('#monynational0').hide();
                $('#monynational1').show();
            }
            else{
                $('#checkbox-toggle8').removeAttr("checked");
                $('#monynational0').show();
                $('#monynational1').hide();
            }
            if($("#checkbox-toggle5").prop("checked") == true){
                $('#checkbox-toggle9').attr("checked","checked");
                $('.hrlnatnpy').show();
            }else{
                 $('#checkbox-toggle9').removeAttr("checked");
                $('.hrlnatnpy').hide();
            }
        }
        else if($(this).prop("checked") == false){
            $('.nationalall').show();
        }
    });
    $('#checkbox-toggle6').on("click",function(){
        if($(this).prop("checked") == true){
            $('#monylocal0').hide();
            $('#monylocal1').show();
        }
        else if($(this).prop("checked") == false){
            $('#monylocal1').hide();
            $('#monylocal0').show();
        }
    }); 
    $('#checkbox-togglec1').on("click",function(){
        if($(this).prop("checked") == true){
            window.location.href = "/new_modules/harmohannew/preferencesteptwo";
        }
        else if($(this).prop("checked") == false){
        }
    });
    $('#checkbox-toggle5').on("click",function(){
        if($(this).prop("checked") == true){
            $('.localhourly').show();
        }
        else if($(this).prop("checked") == false){
            $('.localhourly').hide();
        }
    });
    $('#checkbox-toggle').on("click",function(){
        if($(this).prop("checked") == true){
            $('.withinbody').show();
        }
        else if($(this).prop("checked") == false){
            $('.withinbody').hide();
        }
    });
    $('#checkbox-toggle1').on("click",function(){
        if($(this).prop("checked") == true){
            $('.withinbodycstm').show();
            $('.sftrwithselect').text('');
            $('#withinregion').val('');
            $('.withinbodycstm .col-act a').trigger("click");
        }
        else if($(this).prop("checked") == false){
            $('.withinbodycstm').hide();
            $('#withinregion').val('');
            $('.sftrwithselect').text('');
            $('#modal-location .sftrwithselect').text('');
        }
    });

    $('#checkbox-toggle2').on("click",function(){
        if($(this).prop("checked") == true){
            $('.inwide').show();
        }
        else if($(this).prop("checked") == false){
            $('.inwide').hide();
        }
    });
    $('#checkbox-toggle3').on("click",function(){
        if($(this).prop("checked") == true){
            $('.inoptwide').show();
            $('.sftroutwithselect').text('');
            $('#outsidecountrytxt').val('');
            $('.inoptwide .col-act a').trigger("click");
        }
        else if($(this).prop("checked") == false){
            $('.inoptwide').hide();
            $('#outsidecountrytxt').val('');
            $('.sftroutwithselect').text('');
            $('#modal-outsidelocation .sftroutwithselect').text('');
        }
    });


    $('#checkbox-toggle8').on("click",function(){
        if($(this).prop("checked") == true){
            $('#monynational0').hide();
            $('#monynational1').show();
        }
        else if($(this).prop("checked") == false){
            $('#monynational1').hide();
            $('#monynational0').show();
        }
    }); 
    $('#checkbox-toggle9').on("click",function(){
        if($(this).prop("checked") == true){
            $('.hrlnatnpy').show();
        }
        else if($(this).prop("checked") == false){
            $('.hrlnatnpy').hide();
        }
    });


    $('#checkbox-toggle01').on("click",function(){
        if($(this).prop("checked") == true){
            $('#monyintna0').hide();
            $('#monyintna1').show();
        }
        else if($(this).prop("checked") == false){
            $('#monyintna1').hide();
            $('#monyintna0').show();
        }
    }); 
    $('#checkbox-toggle02').on("click",function(){
        if($(this).prop("checked") == true){
            $('.hrlinnnatnpy').show();
        }
        else if($(this).prop("checked") == false){
            $('.hrlinnnatnpy').hide();
        }
    });


    $('#i_s').on('click',function(){
        var stp= $(this).attr('data');
        if(stp==5)
        {
            if($("#checkbox-toggle").prop("checked") == true)
            {
             var sc=parseInt(stp)+1;
             showScreen(sc);
            }else{
                if($("#checkbox-toggle2").prop("checked") == true)
            {
             var sc=parseInt(stp)+2;
             showScreen(sc);
            }else{
                var sc=parseInt(stp)+3;
             showScreen(sc);
            }
            }

        }
        else if(stp==6)
        {
            if($("#checkbox-toggle2").prop("checked") == true)
            {
                var sc=parseInt(stp)+1;
                showScreen(sc);
            }else{
                var sc=parseInt(stp)+2;
                showScreen(sc);
            }
        }
       
        else
        {
            var sc=parseInt(stp)+1;
            showScreen(sc);
        }
       
    });
    $('#b_s').on('click',function(){
        var stp= $(this).attr('data');
        if(stp==7)
        {
            if($("#checkbox-toggle2").prop("checked") == true)
            {
                var sc=parseInt(stp)-1;
                showScreen(sc);
            }else{
                if($("#checkbox-toggle").prop("checked") == true)
                {
                    var sc=parseInt(stp)-2;
                    showScreen(sc);
                }else{
                    var sc=parseInt(stp)-3;
                    showScreen(sc);
                }
            }
        }
        else if(stp==6)
        {
            if($("#checkbox-toggle").prop("checked") == true)
            {
             var sc=parseInt(stp)-1;
             showScreen(sc);
            }else{
                var sc=parseInt(stp)-2;
             showScreen(sc);
            }
        }
       
        else
        {
             var sc=parseInt(stp)-1;
        showScreen(sc);
        }
    });
   $('.stponecard').on('click',function(){
        var stp= $(this).attr('data');
        if(stp==1){
            $('#card'+stp+">div").toggleClass('card-default');
        $('#card'+stp+">div").toggleClass('card-active');
        $('#card2>div').removeClass( "card-active" );
        $('#card3>div').removeClass('card-active');
        $('#lookingstatus').val('actively_looking');

        }
        if(stp==2)
        {
            $('#card'+stp+">div").toggleClass('card-default');
        $('#card'+stp+">div").toggleClass('card-active');
        
        $('#card1>div').removeClass('card-active');
        $('#card3>div').removeClass('card-active');
        $('#lookingstatus').val('casually_looking');
        }
        if(stp==3)
        {
            $('#card'+stp+">div").toggleClass('card-default');
        $('#card'+stp+">div").toggleClass('card-active');
        
        $('#card1>div').removeClass('card-active');
        $('#card2>div').removeClass('card-active');
        $('#lookingstatus').val('only_open_to_offers');
        }
        
    });
    $('.stptwocard').on('click',function(){
        var stp= $(this).attr('data');
        $('#twocard'+stp+">div").toggleClass('card-default');
        $('#twocard'+stp+">div").toggleClass('card-active');
        $('#twocard'+stp+">div i").toggleClass('fa-check-circle');
        $('#twocard'+stp+">div i").toggleClass('fa-times-circle');
    });
    $('.stpthreecard').on('click',function(){
        var stp= $(this).attr('data');
        if(stp==1){
            $('#threecard2>div').toggleClass('card-default');
            $('#threecard2>div').toggleClass('card-active');
            $('#threecard1>div').toggleClass('card-default');
            $('#threecard1>div').toggleClass('card-active');
        }
        if(stp==2){
            $('#threecard2>div').toggleClass('card-default');
            $('#threecard2>div').toggleClass('card-active');
            $('#threecard1>div').toggleClass('card-default');
            $('#threecard1>div').toggleClass('card-active');
        }
        if(stp==3){
            $('#threecard3>div').toggleClass('card-default');
            $('#threecard3>div').toggleClass('card-active');
            var tgl3= $('#threecard3>div').attr('class');
            
            $('#threecard2>div,#threecard1>div').removeAttr('class');
            $('#threecard2>div').attr('class',tgl3);
            $('#threecard1>div').attr('class',"card card-active");
        }
        
    });
    function showScreen(sc){
        if(sc >= 1 && sc <= 9){
            $('.stps').hide();
            $('#step'+sc).attr('style','display:block !important');
            $('#i_s').removeAttr('data');
            $('#i_s').attr('data',sc);
            $('#b_s').attr('data',sc);
            $('.prgs>div').removeAttr('class');
            $('.prgs>div').attr('class','i-inactive');
            if(sc==1){
                $('#prgs1>div').attr('class','i-active');
                $('#b_s').addClass('s-inactive');
                $('#b_s').removeClass('s-active');
            }
            if(sc==2){
                $('#prgs1>div').attr('class','i-active');
                $('#prgs2>div').attr('class','i-process');
                $('#b_s').addClass('s-active');
                $('#b_s').removeClass('s-inactive');
            }
            if(sc==3){
                $('#prgs1>div').attr('class','i-active');
                $('#prgs2>div').attr('class','i-active');
                $('#b_s').addClass('s-active');
                $('#b_s').removeClass('s-inactive');
            }
            if(sc==4){
                $('#prgs1>div').attr('class','i-active');
                $('#prgs2>div').attr('class','i-active');
                $('#prgs4>div').attr('class','i-process');
                $('#b_s').addClass('s-active');
                $('#b_s').removeClass('s-inactive');
            }
            if(sc==5){
                $('#prgs1>div').attr('class','i-active');
                $('#prgs2>div').attr('class','i-active');
                $('#prgs4>div').attr('class','i-active');
                $('#b_s').addClass('s-active');
                $('#b_s').removeClass('s-inactive');
            }
            if(sc==6){
                $('#prgs1>div').attr('class','i-active');
                $('#prgs2>div').attr('class','i-active');
                $('#prgs4>div').attr('class','i-active');
                $('#prgs6>div').attr('class','i-process');
                $('#b_s').addClass('s-active');
                $('#b_s').removeClass('s-inactive');
            }
            if(sc==7){
                $('#prgs1>div').attr('class','i-active');
                $('#prgs2>div').attr('class','i-active');
                $('#prgs4>div').attr('class','i-active');
                $('#prgs6>div').attr('class','i-active');
                $('#b_s').addClass('s-active');
                $('#b_s').removeClass('s-inactive');
            }
            if(sc==8){
                $('#prgs1>div').attr('class','i-active');
                $('#prgs2>div').attr('class','i-active');
                $('#prgs4>div').attr('class','i-active');
                $('#prgs6>div').attr('class','i-active');
                $('#prgs8>div').attr('class','i-active');
                $('#b_s').addClass('s-active');
                $('#b_s').removeClass('s-inactive');
            }
            if(sc==9){
                var ind=$('.item-categories').length;
                if(ind>0)
                {
                	$('#prefform').submit();
            	}
            	else
            	{

                	$.notify({
    				// options
    					message: 'Please select atleast one profession from the list to continue....' 
					},{
    					// settings
    					type: 'danger'
					});
					showScreen(8);
            	}
            }
            
        }
        
    }

    function getAllCategory(){
        var mainobj;
         $.ajax({
            type: "get",
            url: "/new_modules/harmohannew/getallProfessin",
            data:  {},
            success: function(result){
                var obj = JSON.parse(result);
                var selectedID=$('#categorycts').val();
                var categories = [];
                mainobj=obj;
                $('#categorycts').append('<option value="">Select</option>');
                $.each(obj, function(k,b) {
                    for(var i=0; i<b.length-1;i++){
                       $('#categorycts').append('<option value="'+b[i].id+'">'+b[i].low_name+'</option>');                        
                    }                   
                });
                $('#categorycts').selectpicker('refresh');
                //initialisetypehead(categories,obj);
                
            }

        })

        $('#categorycts').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            var selectedItem = $(this).find("option:selected").val();
            var selectedName = $(this).find("option:selected").html();
            $('#low_func').html('');
            $('#catHeading').text('');
            fillModalDataandShow(selectedItem,selectedName,mainobj);
        }); 

        $(document).on("click", ".updateprof", function(event){
            var sid=$(this).attr('datamainids');
            var mainnames=$(this).attr('datamainnm');
            $('#low_func').html('');
            $('#catHeading').text('');
            fillModalDataandShow(sid,mainnames,mainobj);
        }); 

        //PreSelectProf($('#maindd').val(),mainobj);

    }


    function getAllCountry(){
        var mainobj;
        var country = [];
         $.ajax({
            type: "get",
            url: "/new_modules/harmohannew/getUAECountry",
            data:  {},
            success: function(result){
                var obj = JSON.parse(result);
                var selectedID=$('#countrycodeget').val();
                mainobj=obj;
                $.each(obj, function(k,b) {
                    for(var i=0; i<b.length-1;i++){
                        if(b[i].country_iso_code!=selectedID){
                            country.push({"id":b[i].country_iso_code,"name":b[i].country_name});
                        }                         
                    }                   
                });
            }

        })

        console.log(country);
        
        // Initializing the typeahead
        var inputsh = $('.typeahead').typeahead({
            hint: true,
            highlight: true, /* Enable substring highlighting */
            minLength: 1, /* Specify minimum characters required for showing result */
            source: country,
            
        });

        inputsh.change(function() {
            var a = inputsh.typeahead("getActive");
            if (a) {
            	if(a.name==inputsh.val()){
	                var l =$('.outinbodycstm .sftroutwithselect li').length;
	                var rehtml='<li><input type="hidden" name="whereabouts[international][custom_regions]['+l+'][countryname]" value="'+a.name+'"><input type="hidden" name="whereabouts[international][custom_regions]['+l+'][location_type]" value="country"><input type="hidden" name="whereabouts[international][custom_regions]['+l+'][region_name]" value=""><input type="hidden" name="whereabouts[international][custom_regions]['+l+'][country_iso_code]" value="'+a.id+'"><div class="card c-pill"><span class="close-icon removewithin" id="heelo" data-effect="fadeOut" datavalue="'+a.name+'"><i class="fa fa-times-circle"></i></span><span>'+a.name+'</span></div></li>';
	                $('.sftroutwithselect').append(rehtml);
	                $('.outinbodycstm .col-selected').html(l+' Selected');
	                $('#outsidecountrytxt').val('');
	               	$('.typeahead').typeahead('close');
               	}
            }
            
        });
    }
    $("#modalousideClose").on('click',function(){
    	var l =$('.outinbodycstm .removewithin').length;
    	if(l==0){
        	$("#checkbox-toggle3").prop("checked",false);
        	$('.outinbodycstm').hide();
        	$('#outsidecountrytxt').val('');
        	$('.sftroutwithselect').text('');
    	}
   		$('.outinbodycstm .col-selected').html(l+' Selected');
	});
	$("#modal-outsidelocation").on("hide.bs.modal", function(event) {
    	var l =$('.outinbodycstm .removewithin').length;
    	if(l==0){
            $("#checkbox-toggle3").prop("checked",false);
            $('.outinbodycstm').hide();
            $('#outsidecountrytxt').val('');
            $('.sftroutwithselect').text('');
        }
        $('.outinbodycstm .col-selected').html(l+' Selected');
	});





    function fillModalDataandShow(id,mainnames,c){
        $.each(c, function(k,b) {
            for(var i=0; i<b.length-1;i++){
                if(b[i].id==id){
                    var catanme=b[i].low_sub_category.sub_category_name;
                    var subcatid=b[i].low_sub_category.id;
                    var low_funclitst=b[i].low_sub_category.lows_functions_list;
                    var lowfunchtml;
                    $('.professionmodel  .modal-title').html(b[i].low_name);
                    if(catanme==""){
                        $('#catHeading').text('');
                    }
                    else
                    {
                        $('#catHeading').text(catanme);
                    }
                    for(var k =1;k<low_funclitst.length;k++)
                    {
                        var s="";
                        var ic="display:none;";
                        if($('#proitm'+low_funclitst[k].id).length){
                             s="pro-active";
                             ic="display:block;";
                        }
                        lowfunchtml+="<li class='profsubfunction "+s+"'><div class='profsubfunctions' data-funcid='"+low_funclitst[k].id+"' data-funcname='"+low_funclitst[k].function_name+"' data-maincatid='"+id+"' data-maincatname='"+mainnames+"' data-subcatid='"+subcatid+"'> <i class='fa fa-check-circle' aria-hidden='true' style='"+ic+"'></i> "+low_funclitst[k].function_name+"</div</li>";
                    }
                    $('#low_func').append(lowfunchtml);
                }
            }                   
        });
        $("#mdlclbtn").click();
    }

    // function PreSelectProf(ids,c){
    //     var id;
    //     if(ids!==""){
    //         var res = id.split(",");
    //         for(var o=0;o<=res.length-2;o++){
    //             id=res[o];
    //             $.each(c, function(k,b) {
    //                 for(var i=0; i<b.length-1;i++){
    //                     if(b[i].id==id){
    //                         var catanme=b[i].low_sub_category.sub_category_name;
    //                         var subcatid=b[i].low_sub_category.id;
    //                         var low_funclitst=b[i].low_sub_category.lows_functions_list;
    //                         var lowfunchtml;
    //                         $('.professionmodel  .modal-title').html(b[i].low_name);
    //                         if(catanme==""){
    //                             $('#catHeading').text('');
    //                         }
    //                         else
    //                         {
    //                             $('#catHeading').text(catanme);
    //                         }
    //                         for(var k =1;k<low_funclitst.length;k++)
    //                         {
    //                             var arr = [];
    //                             var lowfunid=low_funclitst[k].id;
    //                             var lowfunname=low_funclitst[k].function_name;
    //                             var mainCat=id;
    //                             var mainCatname=b[i].low_name;
    //                             var subcatid=subcatid;
    //                             arr.push({id:lowfunid,name:lowfunname,subcatid:subcatid});
    //                             bindCarosal(arr,lowfunname,mainCat,mainCatname);
    //                         }
    //                     }
    //                 }                   
    //             });
    //         }


    //     }

    // }

    $(document).on("click", ".deletelistitem", function(event){
        $(this).parents('li').remove();
        var l =$('#mainprof'+$(this).attr('datama')+" ul li").length;
        $('#mainprof'+$(this).attr('datama')+" .col-selected").html(l+' Selected');
        if(l<1){
            var listItem = $('#mainprof'+$(this).attr('datama')).parent('.owl-item');
            var indexToRemove =$( ".owl-item" ).index( listItem );
            $("#categories-slider").trigger('remove.owl.carousel', [indexToRemove]).trigger('refresh.owl.carousel');
            var ln =$('.owl-item').length;
            if(ln<1){
                $('#categories-slider').removeAttr('style');
                $('#categories-slider').attr('style','display:none !important;');
            }
        }
        
    });
    $(document).on("click", ".removewithin", function(event){
        $(this).parents('li').remove();
        var l =$('.withinbodycstm .removewithin').length;
        if(l==0)
        {
            $("#checkbox-toggle1").prop("checked",false);
            $('.withinbodycstm').hide();
            $('#withinregion').val('');
            $('.sftrwithselect').text('');

        }
        $('.withinbodycstm .col-selected').html(l+' Selected');
        
    });

    $(".dgtval").keypress(function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            return false;
        }
    });
    $(".dgtval").change(function () {
        var rs=$(this).val().replace(",", "");
        var a= parseFloat(rs).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $(this).val(c);
    });
    //-----local prefered calculation
    $('#lmmini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#lmpre').val(c);
        var yr=(parseFloat(rs)*12);
        var ypref= ((parseFloat(yr)*20)/100);
        var ycalcc= parseFloat(ypref)+parseFloat(yr);
        var ya= parseFloat(ycalcc).toFixed(2);
        var yra= parseFloat(yr).toFixed(2);
        var yc= ya.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        var myc= yra.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#lymini').val(myc);
        $('#lypre').val(yc);
    });
    $('#lymini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#lypre').val(c);
        var yr=(parseFloat(rs)/12);
        var ypref= ((parseFloat(yr)*20)/100);
        var ycalcc= parseFloat(ypref)+parseFloat(yr);
        var ya= parseFloat(ycalcc).toFixed(2);
        var yra= parseFloat(yr).toFixed(2);
        var yc= ya.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        var myc= yra.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#lmmini').val(myc);
        $('#lmpre').val(yc);
    });
    $('#lhmini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#lhpre').val(c);
    });
    //----------nation prefred calculation
    $('#nmmini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#nmpre').val(c);
        var yr=(parseFloat(rs)*12);
        var ypref= ((parseFloat(yr)*20)/100);
        var ycalcc= parseFloat(ypref)+parseFloat(yr);
        var ya= parseFloat(ycalcc).toFixed(2);
        var yra= parseFloat(yr).toFixed(2);
        var yc= ya.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        var myc= yra.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#nymini').val(myc);
        $('#nypre').val(yc);
    });
    $('#nymini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#nypre').val(c);
        var yr=(parseFloat(rs)/12);
        var ypref= ((parseFloat(yr)*20)/100);
        var ycalcc= parseFloat(ypref)+parseFloat(yr);
        var ya= parseFloat(ycalcc).toFixed(2);
        var yra= parseFloat(yr).toFixed(2);
        var yc= ya.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        var myc= yra.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#nmmini').val(myc);
        $('#nmpre').val(yc);
    });
    $('#nhmini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#nhpref').val(c);
    });
     //----------internation prefred calculation
    $('#inmmini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#inmpre').val(c);
        var yr=(parseFloat(rs)*12);
        var ypref= ((parseFloat(yr)*20)/100);
        var ycalcc= parseFloat(ypref)+parseFloat(yr);
        var ya= parseFloat(ycalcc).toFixed(2);
        var yra= parseFloat(yr).toFixed(2);
        var yc= ya.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        var myc= yra.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#inymini').val(myc);
        $('#inypre').val(yc);

    });
    $('#inymini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#inypre').val(c);
        var yr=(parseFloat(rs)/12);
        var ypref= ((parseFloat(yr)*20)/100);
        var ycalcc= parseFloat(ypref)+parseFloat(yr);
        var ya= parseFloat(ycalcc).toFixed(2);
        var yra= parseFloat(yr).toFixed(2);
        var yc= ya.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        var myc= yra.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#inmmini').val(myc);
        $('#inmpre').val(yc);
    });
    $('#inhmini').on('change',function(){
        var rs=$(this).val().replace(",", "");
        var pref= ((parseFloat(rs)*20)/100);
        var calcc= parseFloat(pref)+parseFloat(rs);
        var a= parseFloat(calcc).toFixed(2);
        var c= a.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        $('#inhpref').val(c);
    });
  	$("#modalLocationClose").on('click',function(){
    	var l =$('.withinbodycstm .removewithin').length;
    	if(l==0){
        	$("#checkbox-toggle1").prop("checked",false);
        	$('.withinbodycstm').hide();
        	$('#withinregion').val('');
        	$('.sftrwithselect').text('');
    	}
   		$('.withinbodycstm .col-selected').html(l+' Selected');
	});
	$("#modal-location").on("hide.bs.modal", function(event) {
    	var l =$('.withinbodycstm .removewithin').length;
    	if(l==0){
            $("#checkbox-toggle1").prop("checked",false);
            $('.withinbodycstm').hide();
            $('#withinregion').val('');
            $('.sftrwithselect').text('');
        }
        $('.withinbodycstm .col-selected').html(l+' Selected');
	});

    $('#seembtndlr').on('click',function(){
        $('#seembtndlr').attr('style','display:none');
        $('#seemore').attr('style','display:block');
    });



    var i;
    var txt;
    var speed = 50;
$('.typeWriter').on('click',function(){
    i = 0;
    document.getElementById("newdemo").innerHTML="";
    $('#newdemo').html('');
    txt="";
    txt=$(this).children().html();
    typeWriter();
    
});

var imageLoader = document.getElementById('getimage');
imageLoader.addEventListener('change', handleImage, false);


$('#imgup').on('change', function () {
    alert();
    var reader = new FileReader();
    reader.onload = function () {
        var thisImage = reader.result;
        localStorage.setItem("imgData", thisImage);
    };
    reader.readAsDataURL(this.files[0]);
});

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("newdemo").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
    $('#upld').hide();
    $('#vid').hide();
  }
  else{
  //alert("complete");
  $('#upld').show();
  }
}
   

    var s2 = $("#unranged-value").freshslider({
        step: 10,
        value:50,
        min: 10,
        max: 250,
        onchange:function(value){
            $(".fscaret").text(value+"km");
            $("#radiusfld").val(value);
            mapRadiusChange(value);
            
        }
    });

    function jobTimer(){
        var alldate = $('#expiry-date').val();
        var initialOffset = '440';
        var i = 1;
        //$('.circle_animation').css('stroke-dashoffset', initialOffset-(1*(initialOffset/time)));

    // Set the date we're counting down to
      var countDownDate = new Date(alldate).getTime();

    // Update the count down every 1 second
      var x = setInterval(function() {
    // Get todays date and time
      var now = new Date().getTime();
    
    // Find the distance between now and the count down date
      var distance = countDownDate - now;
         
    // Time calculations for days, hours, minutes and seconds
      var hours = Math.floor((distance ) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        
      // Output the result in an element with id="demo"
    
      var hrss = $('#hr-js').html(hours + 'h');
      var minss = $('#mi-js').html(minutes + 'm');
      var secss = $('#sec-js').html(seconds + 's');
        
      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        $('.tag-count').html('');
        $('.tag-count').html('Job Expired');
        var hrss = $('#hr-js').remove();
        var minss = $('#mi-js').remove();
        var secss = $('#sec-js').remove();
      }
      if (i == seconds) {    
        i=0;
       initialOffset = '440';
    }
    $('.circle_animation').css('stroke-dashoffset', initialOffset-((i+1)*(initialOffset/seconds)));
    i++;  
    }, 1000);

}



    
})(jQuery);