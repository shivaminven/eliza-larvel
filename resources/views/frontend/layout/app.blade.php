<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ asset('public/assets/images/icons/favicon.ico') }}" />
     <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap-datepicker.min.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap-select.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/main.css') }}">
    <!--===============================================================================================-->
</head>

<body id="{{ $bodyclass }}">
  @section('sidebar')
  @show
  @yield('content')
  <!--===============================================================================================-->
  <script src="{{ asset('public/assets/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/owl.carousel.js') }}"></script>
  <script src="{{ asset('public/assets/js/bootstrap.js') }}"></script>
  <script src="{{ asset('public/assets/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/bootstrap-select.js') }}"></script>
  <script src="{{ asset('public/assets/js/freshslider.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/bootstrap-notify.js') }}"></script>
</body>
</html>