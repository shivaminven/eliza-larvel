@extends('frontend.layout.app')
@section('title', 'Login')
@section('sidebar')
    @parent
@endsection
@section('content')
<div class="container">
  <div class="row">

    <div class="col-sm-9 col-md-6 col-md-offset-3 col-lg-6 topmargin ">
      <div class="text-center">
        <img src="{{ asset('public/assets/images/logo.png')}}" style="width: 25%;">
      </div>
      <div class="card card-signin my-5">
        <div class="card-body">
          <h2 class="card-title">Sign In</h2>
          <form class="form-signin">
            <div class="form-label-group">
              <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
              <label for="inputEmail">Email address</label>
            </div>

            <div class="form-label-group">
              <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
              <label for="inputPassword">Password</label>
            </div>

            <!--div class="custom-control custom-checkbox mb-3">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">Remember password</label>
            </div-->
            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
            <hr class="my-4">
            <a class="btn btn-lg btn-facebook btn-block text-uppercase"> Sign in with Linkedin</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection